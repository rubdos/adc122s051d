use std::net::{TcpListener, TcpStream};
use std::net::SocketAddr;
use std::io::{BufRead, BufReader, Read, Write};

use spidev::{Spidev, SpidevOptions, SpidevTransfer, SpiModeFlags};

use log::*;

use failure::*;

struct Adc122S051D {
    spi: Spidev,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
enum AdcMode {
    Continuous,
    OneShot,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
struct Adc122S051DConfig {
    mode: AdcMode,
    channel: u8,
    window: usize,
    frequency: u32,
}

impl Adc122S051D {
    fn open() -> Result<Adc122S051D, Error> {
        let spi = Spidev::open("/dev/spidev1.0")?;

        Ok(Adc122S051D {
            spi,
        })
    }

    fn batch_read(&mut self, config: &Adc122S051DConfig) -> Result<Vec<u16>, Error> {
        const MAX_WINDOW: usize = 512;

        let mut data = Vec::with_capacity(config.window);
        let mut rx_buf = vec![0u8; config.window * 2];
        let tx_buf = [config.channel << 3, 0x0];
        let tx_buf = std::iter::repeat(&tx_buf).take(MAX_WINDOW).flatten().cloned().collect::<Vec<_>>();

        let mut xfers = Vec::new();

        let mut rx_right: &mut [u8] = &mut rx_buf;
        while rx_right.len() > 0 {
            let len = std::cmp::min(rx_right.len(), MAX_WINDOW);
            let (left, right) = rx_right.split_at_mut(len);
            rx_right = right;

            let mut xfer = SpidevTransfer::read_write(&tx_buf[..left.len()], left);
            xfer.cs_change = 0;
            xfers.push(xfer);
        }

        self.spi.transfer_multiple(&mut xfers)?;

        for i in 0..config.window {
            data.push(((rx_buf[i * 2] as u16) << 8) | rx_buf[i * 2 + 1] as u16);
        }

        Ok(data)
    }

    fn handle_client(&mut self, socket: TcpStream, addr: SocketAddr) -> Result<(), Error> {
        let mut socket = BufReader::new(socket);
        let mut line = String::new();
        ensure!(socket.read_line(&mut line).is_ok());
        let config: Adc122S051DConfig = serde_json::from_str(&line)?;

        info!("New client {} using config {:?}", addr, config);

        let options = SpidevOptions::new()
            .bits_per_word(0)
            .max_speed_hz(config.frequency)
            .mode(SpiModeFlags::SPI_MODE_0)
            .build();

        self.spi.configure(&options)?;

        let mut socket = socket.into_inner();

        let mut null = [0u8; 64];
        match config.mode {
            AdcMode::Continuous => loop {
                let samples = self.batch_read(&config)?;
                writeln!(socket, "{:?}", samples)?;
            },
            AdcMode::OneShot => loop {
                socket.read(&mut null)?;

                let samples = self.batch_read(&config)?;
                writeln!(socket, "{:?}", samples)?;
            },
        }
    }
}

fn main() -> Result<(), Error> {
    let listener = TcpListener::bind("[::1]:8912")?;

    let mut dev = Adc122S051D::open()?;

    env_logger::init();

    loop {
        let (socket, addr) = listener.accept()?;
        match dev.handle_client(socket, addr) {
            Ok(()) => (),
            Err(err) => error!("Client errored; continuing: {}", err),
        }
    }
}
